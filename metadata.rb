name             'gitlab_omnibus'
maintainer       'Drew Blessing'
maintainer_email 'drew.blessing@mac.com'
license          'Apache 2.0'
description      'Installs/Configures GitLab and GitLab CI Omnibus'
long_description 'Installs and configures GitLab and GitLab CI using GitLab Omnibus packages'
version          '1.0.1'

depends 'poise', '~> 1.0'
depends 'packagecloud', '< 1.0'

supports 'centos', '>= 6.5'
supports 'debian', '>= 7.1'
supports 'ubuntu', '>= 12.04'
