#
# Cookbook Name:: gitlab_omnibus
# Recipe:: _ci_backup
#
# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cron 'backup_gitlab_ci' do
  command node['gitlab_omnibus']['ci_backup']['command']
  user node['gitlab_omnibus']['ci_backup']['user']
  minute node['gitlab_omnibus']['ci_backup']['minute']
  hour node['gitlab_omnibus']['ci_backup']['hour']
  day node['gitlab_omnibus']['ci_backup']['day']
  month node['gitlab_omnibus']['ci_backup']['month']
  weekday node['gitlab_omnibus']['ci_backup']['weekday']
end
