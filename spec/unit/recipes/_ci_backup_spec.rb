require 'spec_helper'

describe 'gitlab_omnibus::_ci_backup' do
  context 'with non-default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(platform: 'centos',
                               version: '6.5',
                               file_cache_path: '/var/tmp') do |node|
        node.set['gitlab_omnibus']['ci_backup']['command'] = '/path/to/custom/backup'
        node.set['gitlab_omnibus']['ci_backup']['minute'] = '15'
        node.set['gitlab_omnibus']['ci_backup']['hour'] = '1'
        node.set['gitlab_omnibus']['ci_backup']['day'] = '2'
        node.set['gitlab_omnibus']['ci_backup']['month'] = '3'
        node.set['gitlab_omnibus']['ci_backup']['weekday'] = '0'
      end.converge(described_recipe)
    end
    subject { chef_run }

    it do
      is_expected.to create_cron('backup_gitlab_ci')
        .with_command('/path/to/custom/backup')
        .with_minute('15')
        .with_hour('1')
        .with_day('2')
        .with_month('3')
        .with_weekday('0')
    end
  end
end
